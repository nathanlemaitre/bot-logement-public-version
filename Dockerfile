# syntax=docker/dockerfile:1
FROM python:3.7-alpine
RUN apk add --no-cache gcc musl-dev linux-headers libffi-dev curl
COPY entrypoint.sh entrypoint.sh
RUN ["chmod", "+x", "entrypoint.sh"]
RUN ["chmod", "777", "entrypoint.sh"]
COPY req.txt req.txt
RUN pip install -r req.txt
COPY scrap_logement/ /code
COPY . .
ENTRYPOINT ["sh", "entrypoint.sh"]